import cv2
import numpy as np
import sys
# sys.path.append('/../insightface/detection/scrfd/tools')
# sys.path.insert(1, '/../insightface/detection/scrfd/tools')
sys.path.insert(0, '../detection/scrfd/tools')
from scrfd import *
import time


class VideoCamera(object):
    def __init__(self):
        # 通过opencv获取实时视频流
        self.video = cv2.VideoCapture(0)
    def __del__(self):
        self.video.release()
    def get_frame(self):
        success, image = self.video.read()
        # 因为opencv读取的图片并非jpeg格式，因此要用motion JPEG模式需要先将图片转码成jpg格式图片
        # ret, jpeg = cv2.imencode('.jpg', image)
        # return jpeg.tobytes()
        return image

camera = VideoCamera()
detector = SCRFD(model_file='../detection/scrfd/onnx/scrfd_500m_bnkps.onnx')


while True:
    frame = camera.get_frame()
    for _ in range(1):
        start = time.time()
        bboxes, kpss = detector.detect(frame, 0.5, input_size = (640, 640))
        print("Time process: ", time.time() - start)
        for i in range(bboxes.shape[0]):
            bbox = bboxes[i]
            x1, y1, x2, y2, score = bbox.astype(np.int)
            cv2.rectangle(frame, (x1, y1), (x2, y2), (255, 0, 0), 2)
            if kpss is not None:
                kps = kpss[i]
                for kp in kps:
                    kp = kp.astype(np.int)
                    cv2.circle(frame, tuple(kp), 1, (0, 0, 255), 2)

    cv2.imshow("Frame", frame)
    if cv2.waitKey(25) & 0xff == ord('q'):
        break

camera.video.release()

cv2.destroyAllWindows()